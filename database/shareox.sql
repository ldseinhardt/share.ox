DROP DATABASE IF EXISTS shareox;

CREATE DATABASE shareox;

USE shareox;

source create.sql
source insert.sql
